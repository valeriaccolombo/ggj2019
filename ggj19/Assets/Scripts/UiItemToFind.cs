﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiItemToFind : MonoBehaviour
{
    private GameObject _check;
    public bool Found { get; private set; }
    public int FoundItemId;

    private void Awake()
    {
        _check = transform.Find("Check").gameObject;
        _check.SetActive(false);
        Found = false;
    }

    public void OnFound()
    {
        Found = true;
        _check.SetActive(true);
    }
}
