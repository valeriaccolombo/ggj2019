﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    [SerializeField] private UiItemToFind[] _itemsToFind;
    [SerializeField] private PopupWin _popupWin;
    
    [SerializeField] private AudioClip _itemFound_sfx;
    [SerializeField] private AudioClip _gameWon_sfx;

    [SerializeField] private GameObject _exitPopup;
    [SerializeField] private Button _exitPopupBtnBack;
    [SerializeField] private Button _exitPopupBtnClose;
    
    private bool _gameFinished = false;
    private int _foundItems = 0;
    
    private static Game _instance;

    public static Game Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        _instance = this;
        _exitPopupBtnClose.onClick.AddListener(OnExitPopupExit);
        _exitPopupBtnBack.onClick.AddListener(OnExitPopupBack);
    }

    private void OnExitPopupBack()
    {
        _exitPopup.SetActive(false);
    }

    private void OnExitPopupExit()
    {
        Application.Quit();
    }

    private void Update()
    {
        if (!_gameFinished && Input.GetKeyDown(KeyCode.Escape))
        {
            _exitPopup.SetActive(true);
        }
    }

    public void OnItemFound(int itemId)
    {
        foreach (var item in _itemsToFind)
        {
            if(item.FoundItemId == itemId && !item.Found)
            {
                item.OnFound();
                _foundItems++;
                PlaySfx(_itemFound_sfx);

                if (_foundItems == _itemsToFind.Length)
                {
                    _gameFinished = true;
                    StartCoroutine(ShowWinPopupInAWhile());
                }
            }
        }
    }

    private IEnumerator ShowWinPopupInAWhile()
    {
        var volume = 1f;
        while (volume >= 0)
        {
            volume -= 0.1f;
            SoundManager.Instance.ChangeMusicVolume(volume);
            yield return new WaitForSeconds(0.25f);
        }
        
        PlaySfx(_gameWon_sfx);        
        _popupWin.Show();
        
        yield return new WaitForSeconds(0.5f);
        
        while (volume <= 1)
        {
            volume += 0.1f;
            SoundManager.Instance.ChangeMusicVolume(volume);
            yield return new WaitForSeconds(0.25f);
        }
    }

    public void PlaySfx(AudioClip clip)
    {
        SoundManager.Instance.PlaySfx(clip);
    }
}
