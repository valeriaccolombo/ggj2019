﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PopupWin : MonoBehaviour
{
    [SerializeField] private Button _buttonHome;
    [SerializeField] private Button _buttonExit;
    
    void Start()
    {
        _buttonExit.onClick.AddListener(OnExit);
        _buttonHome.onClick.AddListener(OnHome);
    }

    private void OnExit()
    {
        Application.Quit();
    }

    private void OnHome()
    {
        SceneManager.LoadScene(1);
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }
}
