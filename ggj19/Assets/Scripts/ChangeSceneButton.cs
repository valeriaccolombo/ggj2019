﻿using UnityEngine;

public class ChangeSceneButton : ButtonWithCursor
{
    [SerializeField] private GameObject _sceneFrom;
    [SerializeField] private GameObject _sceneTo;
        
    protected override void OnClick()
    {
        Transition.Instance.PlayTransition(TransitionAction);
    }

    private void TransitionAction()
    {
        _sceneFrom.SetActive(false);
        _sceneTo.SetActive(true);        
    }
}
