﻿using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private static SoundManager _instance;

    public static SoundManager Instance
    {
        get { return _instance; }
    }

    [SerializeField] private AudioSource _musicAudioSource;
    [SerializeField] private AudioSource _sfxAudioSource;
    
    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(_musicAudioSource);
            Destroy(_sfxAudioSource);
            Destroy(this);
        }
        else
        {
            DontDestroyOnLoad(this);
            _instance = this;            
        }
    }

    public void ChangeMusicVolume(float volume)
    {
        _musicAudioSource.volume = volume;
    }

    public void PlaySfx(AudioClip clip)
    {
        _sfxAudioSource.loop = false;
        _sfxAudioSource.clip = clip;
        _sfxAudioSource.Play();
    }
}
