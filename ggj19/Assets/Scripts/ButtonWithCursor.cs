using UnityEngine;
using UnityEngine.UI;

public class ButtonWithCursor : MonoBehaviour
{
    [SerializeField] private Texture2D _handCursor;
    [SerializeField] private bool _tintOnOver = false;

    private Image _imgButton;
    private Color _imgButtonColor;
    
    private void Awake()
    {
        var button = GetComponent<Button>();
        button.onClick.AddListener(OnClick);

        _imgButton = GetComponent<Image>();
        _imgButtonColor = _imgButton.color;
    }

    protected virtual void OnClick()
    {
    }
    
    public void OnMouseOver()
    {
        if (_tintOnOver)
        {
            _imgButtonColor.a = 0.4f;
            _imgButton.color = _imgButtonColor;
        }

        #if UNITY_STANDALONE_WIN
        Cursor.SetCursor (_handCursor, Vector2.zero, CursorMode.Auto);
        #endif   
    }
    public void OnMouseExit()
    {
        if (_tintOnOver)
        {
            _imgButtonColor.a = 0;
            _imgButton.color = _imgButtonColor;
        }
        
        #if UNITY_STANDALONE_WIN
        Cursor.SetCursor (null, Vector2.zero, CursorMode.Auto);
        #endif
    }
}
