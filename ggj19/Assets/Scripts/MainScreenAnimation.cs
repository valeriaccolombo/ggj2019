﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainScreenAnimation : MonoBehaviour
{
    [SerializeField] private GameObject _arrow;
    [SerializeField] private GameObject _text1;
    [SerializeField] private GameObject _text2;
    [SerializeField] private GameObject _text3;
    [SerializeField] private GameObject _cat_pear;
    [SerializeField] private GameObject _buttonPlay;
    [SerializeField] private GameObject _buttonCredits;

    [SerializeField] private AudioClip _qwerty_to_pear_sfx;

    private void Start()
    {
        _arrow.SetActive(false);
        _text1.SetActive(false);
        _text2.SetActive(false);
        _text3.SetActive(false);
        _cat_pear.SetActive(false);
        _buttonPlay.SetActive(false);
        _buttonCredits.SetActive(false);

        StartCoroutine(Animate());
        StartCoroutine(CatAnimation());
    }

    private IEnumerator Animate()
    {
        yield return new WaitForSeconds(1);
        
        _arrow.SetActive(true);
        _text1.SetActive(true);

        yield return new WaitForSeconds(1);

        _text2.SetActive(true);
        
        yield return new WaitForSeconds(1);

        _text3.SetActive(true);

        yield return new WaitForSeconds(1);

        _buttonPlay.SetActive(true);
        _buttonCredits.SetActive(true);
    }

    private IEnumerator CatAnimation()
    {
        _cat_pear.SetActive(false);
        
        while (true)
        {        
            yield return new WaitForSeconds(2f);

            _cat_pear.SetActive(true);
            SoundManager.Instance.PlaySfx(_qwerty_to_pear_sfx);
        
            yield return new WaitForSeconds(0.5f);
        
            _cat_pear.SetActive(false);
            SoundManager.Instance.PlaySfx(_qwerty_to_pear_sfx);

            yield return new WaitForSeconds(0.5f);

            _cat_pear.SetActive(true);
            SoundManager.Instance.PlaySfx(_qwerty_to_pear_sfx);

            yield return new WaitForSeconds(1);

            _cat_pear.SetActive(false);
            SoundManager.Instance.PlaySfx(_qwerty_to_pear_sfx);
        }
    }
}
