﻿using System;
using UnityEngine;

public class Transition : MonoBehaviour
{
    [SerializeField] private AudioClip _transition_sfx;
    
    private Animator _animator;

    private void Awake()
    {
        _instance = this;
        
        _animator = GetComponent<Animator>();
        _animator.Play("loop_open");
    }

    private static Transition _instance;

    public static Transition Instance
    {
        get { return _instance; }
    }

    private Action _callback;
    
    public void PlayTransition(Action callback)
    {
        _callback = callback;
        _animator.Play("close");
        SoundManager.Instance.PlaySfx(_transition_sfx);
    }

    public void OnCloseAnimEnd()
    {
        if (_callback != null)
        {
            _callback();
        }
        _animator.Play("open");
    }
}
