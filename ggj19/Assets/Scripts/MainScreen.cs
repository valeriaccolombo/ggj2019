using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainScreen : MonoBehaviour
{
    [SerializeField] private Button _buttonPlay;
    [SerializeField] private Button _buttonCredits;
    [SerializeField] private Button _buttonExit;    
    
    [SerializeField] private GameObject _creditsPopup;
    [SerializeField] private Button _buttonCloseCredits;
    
    void Start()
    {
        _creditsPopup.SetActive(false);
        
        _buttonPlay.onClick.AddListener(OnPlay);
        _buttonExit.onClick.AddListener(OnExit);
        _buttonCredits.onClick.AddListener(OnCredits);
        _buttonCloseCredits.onClick.AddListener(OnCloseCredits);
    }
    
    private void OnPlay()
    {
        SceneManager.LoadScene(2);
    }
    
    private void OnCredits()
    {
        _creditsPopup.SetActive(true);
    }
    
    private void OnCloseCredits()
    {
        _creditsPopup.SetActive(false);
    }

    private void OnExit()
    {
        Application.Quit();
    }
}
