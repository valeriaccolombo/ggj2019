using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashGGJ : MonoBehaviour
{
    private void Start()
    {
        StartCoroutine(WaitAndContinue());
    }

    private IEnumerator WaitAndContinue()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(1);
    }
}
