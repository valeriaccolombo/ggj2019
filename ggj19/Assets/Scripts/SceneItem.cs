﻿using UnityEngine;

public class SceneItem : ButtonWithCursor
{
    [SerializeField] private int _foundItemId;
    
    protected override void OnClick()
    {
        Game.Instance.OnItemFound(_foundItemId);
    }
}
